// app.js
App({
  onLaunch() {
    // 展示本地存储能力
    const logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        // 以form提交方式请求accesstoken
        wx.request({
          url: 'http://127.0.0.1:8080/accesstoken',
          method: 'POST',
          data:
            //数据urlencode方式编码，变量间用&连接，再post
            'code=' + res.code,
          method: 'POST',
          header: {
            'content-type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json;charset=UTF-8'
          },
          success: res => {
            console.log(res);
            this.globalData.accessToken = res.data.accesstoken;
            wx.setStorageSync('accessToken', res.data.accesstoken)
            console.log('accessToken: ' + this.globalData.accessToken);
            if (this.accessTokenReadCallBack) {
              this.accessTokenReadCallBack(this.globalData.accessToken)
            }
          },
        })
      }
    })
    
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo
              console.log(this.globalData.userInfo);
              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  },
  globalData: {
    userInfo: null,
    accessToken: null
  }
})