package cn.edu.dgut.css.sai.demo.wechat.miniprogramuaa;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MiniProgramUaaApplicationTests {

    @Autowired
    MiniProgramUaaApplication.AccessTokenEndpoint accessTokenEndpoint;

    @Test
    void createAccessTokenTest() {
        MiniProgramUaaApplication.SessionDto sessionDto = new MiniProgramUaaApplication.SessionDto();
        sessionDto.setOpenid("openid_测试");
        sessionDto.setSession_key("session_key_测试");
        System.out.println("jwts = " + accessTokenEndpoint.createJwt(sessionDto));
    }

}
