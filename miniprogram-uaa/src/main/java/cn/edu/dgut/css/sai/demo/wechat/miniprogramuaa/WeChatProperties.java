package cn.edu.dgut.css.sai.demo.wechat.miniprogramuaa;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author sai
 * @since 2020/12/2
 */
@Data
@ConfigurationProperties(prefix = "sai.wechat")
public class WeChatProperties {

    private MiniProgram miniProgram;

    @Data
    static class MiniProgram {

        private String code2SessionApi = "https://api.weixin.qq.com/sns/jscode2session?appid={APPID}&secret={SECRET}&js_code={JSCODE}&grant_type=authorization_code";
        private String accessTokenApi = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={APPID}&secret={APPSECRET}";
        private String uniformMessageSendApi = "https://api.weixin.qq.com/cgi-bin/message/wxopen/template/uniform_send?access_token={ACCESS_TOKEN}";

        /**
         * 小程序AppId
         */
        private String AppId;

        /**
         * 小程序AppSecret
         */
        private String AppSecret;
    }

}
